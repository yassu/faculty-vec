#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import json
import statistics as stat
from typing import Dict, List, Union

from gensim.models import KeyedVectors
from json5 import load as _json5_load

MODEL = KeyedVectors.load_word2vec_format("data/model.vec")


def get_similarity(words1: List[str], words2: List[str]) -> float:
    return sum([MODEL.similarity(w1, w2) for w1 in words1 for w2 in words2
               ]) / (len(words1) * len(words2))


def get_similarity_mat(words1: List[List[str]], words2: List[List[str]]) -> List[List[str]]:
    return [[get_similarity(w1, w2) for w1 in words1] for w2 in words2]


def get_similarity_dict(words1: List[List[str]], words2: List[List[str]]):
    return {','.join(w1): {','.join(w2): get_similarity(w1, w2) for w2 in words2} for w1 in words1}


def get_z_value(item, l):
    mu = stat.mean(l)
    sigma = stat.stdev(l)
    return (item - mu) / sigma


def convert_z(similarities: Dict[str, Dict[str, float]]) -> Dict[str, Dict[str, float]]:
    scores = {}
    for x in similarities:
        scores[x] = {}
        for y, score in similarities[x].items():
            # breakpoint()
            targets = [similarities[x0][y] for x0 in similarities.keys()]
            scores[x][y] = get_z_value(similarities[x][y], targets)

    return scores


def save_json_as_relation(names: List[List[str]], out_file) -> None:
    similarities = get_similarity_dict(names, names)
    json.dump(similarities, out_file, indent=4, ensure_ascii=False)


def save_converted_json_as_relation(names: List[List[str]], out_file) -> None:
    similarities = convert_z(get_similarity_dict(names, names))
    json.dump(similarities, out_file, indent=4, ensure_ascii=False)


def split_by_comma(names: List[str]) -> List[List[str]]:
    return list(map(lambda s: s.split(','), names))


def main():
    faculties = _json5_load(open('data/faculty-subject.json5'))
    with open('faculties.json', 'w') as f:
        save_json_as_relation(split_by_comma(list(faculties.keys())), f)
    with open('faculties_z.json', 'w') as f:
        save_converted_json_as_relation(split_by_comma(list(faculties.keys())), f)

    subjects = sum(faculties.values(), [])
    with open('subjects.json', 'w') as f:
        save_json_as_relation(subjects, f)
    with open('subjects_z.json', 'w') as f:
        save_converted_json_as_relation(subjects, f)


if __name__ == '__main__':
    main()
