#!/usr/bin/env python
# -*- coding: utf-8 -*-

import term
from invoke import task


def print_title(title: str):
    term.writeLine("running " + title, term.green)


@task
def format(ctx):
    ctx.run("isort faculty_vec.py")
    ctx.run('yapf --in-place faculty_vec.py')


@task
def test(ctx, with_time=False):
    """ テストする """
    print_title("flake8")
    ctx.run("flake8")

    print_title("isort")
    ctx.run("isort --check-only faculty_vec.py")
